from django.shortcuts import (redirect, render)
from .forms import (UserRegisterForm, UserLoginForm)
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User
from .models import TelegramProfile
from django.shortcuts import get_object_or_404
from django.http import Http404


def register(request):
    template_name = 'auth/register.html'
    form = UserRegisterForm()
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('/')
    return render(request, template_name, {'form': form})


def login_user(request):
    template_name = 'auth/login.html'
    form = UserLoginForm()
    if request.method == 'POST':
        form = UserLoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username=username, password=password)
            if user:
                login(request, user)
                return redirect('/')
    return render(request, template_name, {'form': form})


def logout_user(request):
    logout(request)
    return redirect("login")


def telegram_profile(request):
    if request.user.is_authenticated:
        tg_pf = get_object_or_404(TelegramProfile, user=request.user)
        if request.method == 'POST':
            if request.POST.get('action') == 'activate':
                tg_pf.is_active = True
                tg_pf.save()
            elif request.POST.get('action') == 'delete':
                tg_pf.delete()
                return redirect('main')
        return render(request, 'auth/tg_profile.html', {'tg_pf': tg_pf})
    return Http404()
