from django.urls import path
from .views import (register, login_user, logout_user, telegram_profile)

urlpatterns = [
    path('register/', register, name='register'),
    path('login/', login_user, name='login'),
    path('logout/', logout_user, name='logout'),
    path('telegram_profile/', telegram_profile, name='telegram_profile')
]
