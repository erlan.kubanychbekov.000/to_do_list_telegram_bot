from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()

STATUS_TASK = (
    ('to_do', 'To do'),
    ('doing', 'Doing'),
    ('done', 'Done')
)


class Task(models.Model):
    """Объект для планирования задач"""
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_tasks')
    title = models.CharField(max_length=100)
    description = models.TextField()
    date = models.DateTimeField(auto_now=True)
    status = models.CharField(choices=STATUS_TASK, default=STATUS_TASK[0][0], max_length=10)

    def __str__(self):
        return self.title
