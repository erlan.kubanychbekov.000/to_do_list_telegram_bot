from django.shortcuts import render, redirect
from .models import Task
from .forms import UpdateTaskForm, CreateTaskForm
from django.contrib.auth import get_user_model
from django.http import Http404
from django.contrib import messages
from django.shortcuts import get_object_or_404

User = get_user_model()


def main(request):
    if request.user.is_authenticated:
        user_list = User.objects.all()
        return render(request, 'task/main.html', {'user_list': user_list})
    return redirect('login')


def get_users_task(request, user_id):
    user = User.objects.prefetch_related('user_tasks').filter(id=user_id).first()
    if user is not None:
        return render(request, 'task/users_tasks.html', {'user': user})
    return Http404()


def single_task(request, task_id):
    task = Task.objects.select_related('user').filter(id=task_id).first()
    if task is not None:
        if request.method == 'POST':
            form = UpdateTaskForm(instance=task, data=request.POST)
            if form.is_valid():
                form.save()
                messages.success(request, 'task is updated successfully!')
        form = UpdateTaskForm(instance=task)
        return render(request, 'task/single_task.html', {'form': form})
    return Http404()


def create_task(request):
    if request.method == 'POST':
        form = CreateTaskForm(data=request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'created task')
        return redirect('main')
    form = CreateTaskForm()
    return render(request, 'task/create_task.html', {'form': form})


def delete_task(request, task_id):
    task = get_object_or_404(Task, id=task_id)
    task.delete()
    return redirect('main')
