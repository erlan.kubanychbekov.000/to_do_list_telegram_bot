from django import forms
from .models import Task, STATUS_TASK
from django.contrib.auth import get_user_model

User = get_user_model()


class UpdateTaskForm(forms.ModelForm):
    status = forms.ChoiceField(choices=STATUS_TASK, widget=forms.Select(attrs={'class': 'form-control'}))
    title = forms.CharField(
        widget=forms.TextInput
        (attrs={'class': 'form-control'}))
    description = forms.CharField(
        widget=forms.Textarea
        (attrs={'class': 'form-control'}))

    class Meta:
        model = Task
        fields = ['status', 'title', 'description']


class CreateTaskForm(forms.ModelForm):
    user = forms.ModelChoiceField(queryset=User.objects.all(), widget=forms.Select(attrs={'class': 'form-control'}))
    title = forms.CharField(
        widget=forms.TextInput
        (attrs={'class': 'form-control'}))
    description = forms.CharField(
        widget=forms.Textarea
        (attrs={'class': 'form-control'}))

    class Meta:
        model = Task
        fields = ['user', 'title', 'description']
