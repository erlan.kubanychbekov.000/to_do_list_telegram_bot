from django.urls import path
from .views import (main, get_users_task,
                    single_task, delete_task,
                    create_task
                    )

urlpatterns = [
    path('', main, name='main'),
    path('user/tasks/<int:user_id>/', get_users_task, name='get_users_task'),
    path('tasks/<int:task_id>/', single_task, name='single_task'),
    path('tasks/create/', create_task, name='create_task'),
    path('tasks/delete/<int:task_id>/', delete_task, name='delete_task')
]
