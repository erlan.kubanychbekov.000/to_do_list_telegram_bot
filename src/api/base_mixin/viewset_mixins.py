from rest_framework.viewsets import mixins, ViewSet
from rest_framework.generics import GenericAPIView


class CreateDestroyUpdateMixin(ViewSet,
                               GenericAPIView,
                               mixins.RetrieveModelMixin,
                               mixins.DestroyModelMixin,
                               mixins.CreateModelMixin,
                               mixins.UpdateModelMixin):
    pass


class ReadOnlyMixin(ViewSet,
                    GenericAPIView,
                    mixins.RetrieveModelMixin,
                    mixins.ListModelMixin):
    pass
