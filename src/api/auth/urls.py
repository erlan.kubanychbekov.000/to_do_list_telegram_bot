from django.urls import path
from .views import LoginView, RegisterView, CheckTelegramProfile, CreateTelegramProfile
from rest_framework_simplejwt import views as jwt_views

urlpatterns = [
    path('login/', LoginView.as_view()),
    path('register/', RegisterView.as_view()),
    path('check/telegram_profile/', CheckTelegramProfile.as_view()),
    path('create/telegram_profile/', CreateTelegramProfile.as_view()),
    path('token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
]
