from rest_framework import serializers
from rest_framework import status
from rest_framework.response import Response
from django.contrib.auth import authenticate
from django.contrib.auth import get_user_model
from apps.account.models import TelegramProfile

User = get_user_model()


class LoginSerializer(serializers.Serializer):
    username = serializers.CharField(write_only=True, required=True)
    password = serializers.CharField(write_only=True, required=True)

    def validate(self, attrs):
        username = attrs.get('username')
        password = attrs.get('password')

        if username and password:
            user = authenticate(request=self.context.get('request'),
                                username=username, password=password)
            if not user:
                msg = 'Access denied: wrong username or password.'
                raise serializers.ValidationError(msg, code='authorization')
        else:
            msg = 'Both "username" and "password" are required.'
            raise serializers.ValidationError(msg, code='authorization')
        attrs['user'] = user
        return attrs


class RegisterSerializer(serializers.Serializer):
    username = serializers.CharField(required=True)
    password1 = serializers.CharField(required=True)
    password2 = serializers.CharField(required=True)

    def validate(self, attrs):
        username = attrs.get('username')
        password1 = attrs.get('password')
        password2 = attrs.get('password')

        if password1 != password2:
            msg = 'Access denied: wrong username or password.'
            raise serializers.ValidationError(msg, code='authorization')
        if User.objects.filter(username=username).exists():
            msg = 'Access denied: user already exists.'
            raise serializers.ValidationError(msg, code='authorization')
        return attrs

    def create(self, validated_data):
        username = validated_data['username']
        password = validated_data['password1']
        user = User.objects.create(username=username)
        user.set_password(password)
        user.save()
        return user


class TelegramProfileSerializer(serializers.Serializer):
    telegram_id = serializers.IntegerField(required=True)
    username = serializers.CharField(required=True)

    def save(self, **kwargs):
        telegram_id = self.validated_data['telegram_id']
        username = self.validated_data['username']
        user = User.objects.filter(username=username).first()
        if user is not None:
            tg_pf = TelegramProfile.objects.create(telegram_id=telegram_id, user=user)
            return tg_pf
        else:
            raise serializers.ValidationError({'user': 'User is not found'})

    def validate_telegram_id(self, value):
        if TelegramProfile.objects.filter(user__telegram_profile__telegram_id=value).exists():
            raise serializers.ValidationError({'username': 'already exists'})
        return value

    def validate_username(self, value):
        user = User.objects.filter(username=value).first()
        if user is None:
            raise serializers.ValidationError({'user': 'User is not found'})
        return user
