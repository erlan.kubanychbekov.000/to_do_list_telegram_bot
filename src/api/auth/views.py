from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework import status
from .serializers import LoginSerializer, RegisterSerializer, TelegramProfileSerializer
from django.contrib.auth import login
from django.contrib.auth import get_user_model
from apps.account.models import TelegramProfile

User = get_user_model()


class LoginView(APIView):
    permission_classes = [AllowAny, ]
    serializer_class = LoginSerializer

    def post(self, request):
        serializer = self.serializer_class(data=self.request.data, context={'request': self.request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        login(request, user)
        return Response(None, status=status.HTTP_202_ACCEPTED)


class RegisterView(APIView):
    permission_classes = [AllowAny, ]
    serializer_class = RegisterSerializer

    def post(self, request):
        serializer = self.serializer_class(data=self.request.data, context={'request': self.request})
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            login(request, serializer.instance)
            return Response(status=status.HTTP_201_CREATED)
        return Response(status=status.HTTP_400_BAD_REQUEST)


class AuthByTelegram(APIView):
    permission_classes = [AllowAny, ]

    def post(self, request):
        telegram_id = request.data.get('telegram_id')
        user = User.objects.filter(telegram_id == telegram_id).exists()
        if user:
            raise


class CheckTelegramProfile(APIView):
    permission_classes = [AllowAny, ]

    def get(self, request):
        telegram_id = self.request.query_params.get('telegram_id')
        telegram_profile_exists = TelegramProfile.objects.filter(telegram_id=telegram_id).exists()
        return Response({'user': telegram_profile_exists})


class CheckUser(APIView):
    permission_classes = [AllowAny, ]

    def get(self, request):
        username = self.request.query_params.get('username')
        user_exists = User.objects.filter(username=username).exists()
        return Response({'user': user_exists})


class CreateTelegramProfile(APIView):
    permission_classes = [AllowAny, ]
    serializer_class = TelegramProfileSerializer

    def post(self, request):
        serializer = self.serializer_class(data=self.request.data, context={'request': request})
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(status=status.HTTP_400_BAD_REQUEST)
