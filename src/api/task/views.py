from apps.task.models import Task
from .serializers import ListTaskSerializer, TaskCRUDSerializer
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from ..base_mixin.viewset_mixins import CreateDestroyUpdateMixin, ReadOnlyMixin
from .service import TaskService


class TaskListViewSet(ReadOnlyMixin):
    queryset = Task.objects.all()
    permission_classes = [AllowAny, ]
    serializer_class = ListTaskSerializer

    def get_user_tasks(self, request, pk):
        data = TaskService.get_users_task_by_pk(pk)
        return Response(data)

    def get_telegram_user_tasks(self, request, telegram_id):
        data = TaskService.get_users_task_by_telegram_id(telegram_id)
        return Response(data)


class TaskCUDViewSet(CreateDestroyUpdateMixin):
    permission_classes = [AllowAny, ]
    queryset = Task.objects.all()
    serializer_class = TaskCRUDSerializer
