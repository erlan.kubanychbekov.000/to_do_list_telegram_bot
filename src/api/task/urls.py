from django.urls import path
from .views import TaskListViewSet, TaskCUDViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'task/list', TaskListViewSet, basename='task_list')
router.register(r'task', TaskCUDViewSet, basename='task')

urlpatterns = [
    path('user/task/<int:pk>', TaskListViewSet.as_view({'get': 'get_user_tasks'})),
    path('telegram/task/<int:telegram_id>', TaskListViewSet.as_view({'get': 'get_telegram_user_tasks'}))
]
urlpatterns += router.urls
