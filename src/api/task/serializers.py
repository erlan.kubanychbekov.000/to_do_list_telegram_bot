from rest_framework import serializers
from apps.task.models import Task, STATUS_TASK
from django.contrib.auth import get_user_model

User = get_user_model()


class ListTaskSerializer(serializers.ModelSerializer):
    status = serializers.CharField(source='get_status_display')

    class Meta:
        model = Task
        fields = ['id', 'title', 'description', 'date', 'status']


class TaskCRUDSerializer(serializers.ModelSerializer):
    status = serializers.ChoiceField(choices=STATUS_TASK, required=False)
    telegram_id = serializers.CharField(required=False)

    class Meta:
        model = Task
        fields = ['telegram_id', 'title', 'description', 'status']

    def create(self, validated_data):
        telegram_id = validated_data.get('telegram_id')
        request = self.context.get("request")
        user = request.user
        if telegram_id:
            telegram_id = validated_data.pop('telegram_id')
            user = User.objects.filter(telegram_profile__telegram_id=telegram_id).first()
            task = Task.objects.create(**validated_data, user=user)
            return task
        if user.is_authenticated:
            user = request.user
            task = Task.objects.create(**validated_data, user=user)
            return task
        msg = 'Auth error: 401'
        raise serializers.ValidationError(msg, code='authorization')
