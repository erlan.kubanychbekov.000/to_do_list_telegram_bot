from .serializers import ListTaskSerializer
from apps.task.models import Task


class TaskService:
    @staticmethod
    def get_users_task_by_pk(pk):
        query = Task.objects.filter(user__pk=pk)
        serializer = ListTaskSerializer(query, many=True)
        return serializer.data

    @staticmethod
    def get_users_task_by_telegram_id(telegram_id):
        query = Task.objects.filter(user__telegram_profile__telegram_id=telegram_id)
        print(query)
        serializer = ListTaskSerializer(query, many=True)
        return serializer.data
