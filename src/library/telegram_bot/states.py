from aiogram.dispatcher.filters.state import State, StatesGroup


class ConnectAccountSite(StatesGroup):
    username = State()


class CreateTaskState(StatesGroup):
    title = State()
    description = State()
