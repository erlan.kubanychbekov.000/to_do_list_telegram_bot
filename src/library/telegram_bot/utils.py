import aiohttp


async def request(url, method='GET', data=None):
    async with aiohttp.ClientSession() as session:
        async with session.request(method, url, json=data) as response:
            status = response.status
            json_data = await response.json()
            return status, json_data
