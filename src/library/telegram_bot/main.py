import aiohttp
from aiogram import Bot, Dispatcher, executor, types
from aiogram.dispatcher import FSMContext
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from config import KEY, API_URL
from utils import request
from states import ConnectAccountSite, CreateTaskState

storage = MemoryStorage()
bot = Bot(KEY)
dp = Dispatcher(bot, storage=storage)


@dp.message_handler(commands=['start'])
async def start(message: types.Message):
    url = f'{API_URL}/auth/api/check/telegram_profile/?telegram_id={message.from_user.id}'
    status, json_task_list = await request(url)
    markup = types.InlineKeyboardMarkup()
    connect_acc_btn = types.InlineKeyboardButton('Привязать акканут к сайту', callback_data='connect_to_account')
    reg_link_btn = types.InlineKeyboardButton('Пройти регистрацию', url=f'{API_URL}/auth/register/')
    if not json_task_list['user']:
        msg = f'Пожалуйста, пройдите регистрацию и привяжите аккаунт в To Do:'
        markup.row(reg_link_btn, connect_acc_btn)
        return await message.answer(msg, reply_markup=markup)
    btn1 = types.InlineKeyboardButton('Создать таску', callback_data='create_task')
    btn2 = types.InlineKeyboardButton('Мои таски', callback_data='my_task')
    markup.row(btn1, btn2)
    await message.answer('Вас приветсвует To Do бот!', reply_markup=markup)


@dp.callback_query_handler(lambda c: c.data == 'connect_to_account')
async def connect_to_account_site_callback(callback_query: types.CallbackQuery):
    await bot.answer_callback_query(callback_query.id)
    await bot.send_message(callback_query.from_user.id, 'Введите ник аккаунта из сайта:')
    await ConnectAccountSite.username.set()


@dp.message_handler(state=ConnectAccountSite.username)
async def process_account_connect(message: types.Message, state: FSMContext):
    username = message.text
    payload = {
        'username': username,
        'telegram_id': message.from_user.id,
    }
    url = f'{API_URL}/auth/api/create/telegram_profile/'
    try:
        status, response = await request(url, method='POST', data=payload)
        if status == 201:
            msg = 'Войдите в свой аккаунт и примите запрос'
            await message.answer(msg)
        else:
            msg = 'Пользователь с таким username не найден'
            await message.answer(msg)
    except Exception as e:
        print('Error:', str(e))
        await message.answer('Произошла ошибка при обработке запроса.')

    await state.finish()


@dp.callback_query_handler(lambda c: c.data.startswith('my_task'))
async def my_task_call_back(call):
    url = f'{API_URL}/task/api/telegram/task/{call.message.chat.id}'
    status, json_task_list = await request(url)
    for task in json_task_list:
        text = f'<i>{task["title"]}</i>, <b>status</b>:{task["status"]}'
        button = types.InlineKeyboardButton(text=task["title"], url=f"{API_URL}/tasks/{task['id']}/")
        keyboard = types.InlineKeyboardMarkup().add(button)
        await call.message.answer(text, parse_mode='HTML', reply_markup=keyboard)


@dp.callback_query_handler(lambda c: c.data == 'create_task')
async def create_task_callback(callback_query: types.CallbackQuery):
    await bot.answer_callback_query(callback_query.id)
    await bot.send_message(callback_query.from_user.id, 'Введите название задачи:')
    await CreateTaskState.title.set()


@dp.message_handler(state=CreateTaskState.title)
async def process_task_title(message: types.Message, state: FSMContext):
    task_title = message.text
    await state.update_data(title=task_title)
    await bot.send_message(message.from_user.id, 'Введите описание задачи:')
    await CreateTaskState.description.set()


@dp.message_handler(state=CreateTaskState.description)
async def process_task_description(message: types.Message, state: FSMContext):
    task_description = message.text
    await state.update_data(description=task_description)

    task_data = await state.get_data()
    task_title = task_data.get('title')
    task_description = task_data.get('description')
    url = f'{API_URL}/task/api/task/'
    payload = {
        'title': task_title,
        'description': task_description,
        'telegram_id': message.from_user.id,
        'status': 'to_do'
    }
    async with aiohttp.ClientSession() as session:
        async with session.post(url, json=payload) as response:
            if response.status == 201:
                await bot.send_message(message.from_user.id, 'Задача успешно создана!')
            else:
                await bot.send_message(message.from_user.id, 'Ошибка при создании задачи.')

    await state.finish()


executor.start_polling(dp)
