from decouple import config

KEY = config('TG_KEY')
API_URL = config('DJANGO_HOST')
