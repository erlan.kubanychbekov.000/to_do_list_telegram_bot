from django.contrib import admin
from django.urls import path, include
from .yasg import urlpatterns as doc_urls

urlpatterns = [
    path('', include('apps.task.urls')),
    path('auth/', include('apps.account.urls')),
    path('auth/api/', include('api.auth.urls')),
    path('task/api/', include('api.task.urls')),
    path('admin/', admin.site.urls),
    path('__debug__/', include('debug_toolbar.urls')),
]

urlpatterns += doc_urls
