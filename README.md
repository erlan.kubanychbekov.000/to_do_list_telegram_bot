# To Do App + Telegram bot

## Как установить

Настройка переменных окружения  
1. Скопируйте файл .env.dist в .env
2. Заполните .env файл. Пример:  

Установка виртуального окружения для среды разработки на примере ОС Linux
```shell
virtualenv env
source venv/bin/activate
pip install -r requirements.txt
```
Запустите миграцию
```shell
python manage.py migrate
```
## Как запустить web-сервер
Запуск сервера производится в активированном локальном окружение из папки `src/`
```shell
python manage.py runserver
```

## Как запустить телеграм бота
Запуск сервера производится в активированном локальном окружение из папки `src/`
```shell
python src/library/telegram_bot/main.py
```


## API
Документация API
```shell
http://127.0.0.1:8000/swagger/
```
